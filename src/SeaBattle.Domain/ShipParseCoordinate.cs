﻿using System;

namespace SeaBattle.Domain
{
    public sealed class ShipParseCoordinate
    {
        public Coordinate Start { get; set; }
        public Coordinate End { get; set; }
        
        public ShipParseCoordinate(Coordinate start, Coordinate end)
        {
            Start = start ?? throw new ArgumentNullException(nameof(start));
            End   = end ?? throw new ArgumentNullException(nameof(end));
        }
    }
}