﻿using System.Threading.Tasks;
using SeaBattle.Domain.Entity;

namespace SeaBattle.Domain.Abstractions
{
    public interface IShotRepository
    {
        Task<ShotStatEntity> CreateAsync(Coordinate coordinate);
    }
}