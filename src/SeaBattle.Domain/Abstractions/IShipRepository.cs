﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SeaBattle.Domain.Data;

namespace SeaBattle.Domain.Abstractions
{
    public interface IShipRepository
    {
        Task CreateAsync(List<ShipData> shipData);
    }
}