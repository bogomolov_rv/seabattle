﻿using System.Threading.Tasks;
using SeaBattle.Domain.Entity;

namespace SeaBattle.Domain.Abstractions
{
    public interface IGameRepository
    {
        Task       CreateAsync(int range);
        Task<GameEntity> GetAsync();
        Task<int> DeleteAsync();
    }
}