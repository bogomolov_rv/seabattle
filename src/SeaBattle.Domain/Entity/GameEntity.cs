﻿namespace SeaBattle.Domain.Entity
{
    public sealed class GameEntity
    {
        public int   ID        { get; set; }
        public byte  Range     { get; set; }
        public short ShotCount { get; set; }
        public byte  ShitCount { get; set; }
        public byte  Destroyed { get; set; }
        public byte  Knocked   { get; set; }
    }
}