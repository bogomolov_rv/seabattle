﻿namespace SeaBattle.Domain.Entity
{
    public sealed class ShotStatEntity
    {
        public bool Destroy { get; set; }
        public bool Knock   { get; set; }
        public bool End   { get; set; }
    }
}