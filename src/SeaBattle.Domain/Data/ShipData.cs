﻿using System;
using System.Collections.Generic;

namespace SeaBattle.Domain.Data
{
    public sealed class ShipData
    {
        public ShipData(ShipParseCoordinate parseCoordinate)
        {
            Ship = new List<Coordinate>(0);
            FillShipCoordinates(parseCoordinate);
        }

        public List<Coordinate> Ship { get; set; }

        private void FillShipCoordinates(ShipParseCoordinate parseCoordinate)
        {
            var maxX = Math.Max(parseCoordinate.End.X, parseCoordinate.Start.X);
            var minX = Math.Min(parseCoordinate.End.X, parseCoordinate.Start.X);

            var maxY = Math.Max(parseCoordinate.End.Y, parseCoordinate.Start.Y);
            var minY = Math.Min(parseCoordinate.End.Y, parseCoordinate.Start.Y);

            for (int i = minX; i <= maxX; i++)
            {
                for (int j = minY; j <= maxY; j++)
                {
                    Ship.Add(new Coordinate(i, j));
                }
            }
        }
    }
}