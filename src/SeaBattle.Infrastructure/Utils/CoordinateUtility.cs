﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SeaBattle.Domain;

namespace SeaBattle.Infrastructure.Utils
{
    public static class CoordinateUtility
    {
        public static bool TryParse(string input, out List<ShipParseCoordinate> result)
        {
            result = new List<ShipParseCoordinate>(0);

            if (string.IsNullOrEmpty(input))
            {
                return false;
            }

            var shipsCoordinates = input.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var shipCoordinateStr in shipsCoordinates)
            {
                var coordinateSplit = shipCoordinateStr.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (coordinateSplit.Length > 2)
                {
                    return false;
                }

                if (!TrySingleParse(coordinateSplit[0], out var startCoordinate))
                {
                    return false;
                }
                
                if (!TrySingleParse(coordinateSplit[1], out var endCoordinate))
                {
                    return false;
                }

                result.Add(new ShipParseCoordinate(
                    new Coordinate(startCoordinate.x, startCoordinate.y), 
                    new Coordinate(endCoordinate.x, endCoordinate.y)));
            }

            return result.Any();
        }

        /* format 122А */
        public static bool TrySingleParse(string coordinatePair, out (int x, int y) result)
        {
            result.x = 0;
            result.y = 0;

            StringBuilder sb = new StringBuilder(0);
            char letter = ' ';

            for (int i = 0; i < coordinatePair.Length; i++)
            {
                if (!char.IsLetter(coordinatePair[i]))
                {
                    sb.Append(coordinatePair[i]);
                    continue;
                }

                if (i + 1 < coordinatePair.Length)
                {
                    return false;
                }

                letter = coordinatePair[i];
                break;
            }

            return AlphabetRu.TryGetValue(letter, out result.y) && 
                   (int.TryParse(sb.ToString(), out result.x) && result.x > 0);
        }


        private static readonly Dictionary<char, int> AlphabetRu = new Dictionary<char, int>()
        {
            ['А'] = 1,
            ['Б'] = 2,
            ['В'] = 3,
            ['Г'] = 4,
            ['Д'] = 5,
            ['Е'] = 6,
            ['Ж'] = 7,
            ['З'] = 8,
            ['И'] = 9,
            ['К'] = 10,
            ['Л'] = 11,
            ['М'] = 12,
            ['Н'] = 13,
            ['О'] = 14,
            ['П'] = 15,
            ['Р'] = 16,
            ['С'] = 17,
            ['Т'] = 18,
            ['У'] = 19,
            ['Ф'] = 20,
            ['Х'] = 21,
            ['Ц'] = 22,
            ['Ч'] = 23,
            ['Ш'] = 24,
            ['Щ'] = 25,
            ['Ы'] = 26,
            ['Э'] = 27,
            ['Ю'] = 28,
            ['Я'] = 29
        };
    }
}