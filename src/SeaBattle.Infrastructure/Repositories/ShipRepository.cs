﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;
using SeaBattle.Domain.Abstractions;
using SeaBattle.Domain.Data;

namespace SeaBattle.Infrastructure.Repositories
{
    public class ShipRepository : IShipRepository
    {
        private readonly string _connectionString;

        public ShipRepository(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            _connectionString = connectionString;
        }

        public async Task CreateAsync(List<ShipData> shipData)
        {
            if (shipData == null || !shipData.Any())
            {
                throw new ArgumentNullException(nameof(shipData));
            }

            var json = JsonConvert.SerializeObject(shipData);

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync()
                    .ConfigureAwait(false);

                await connection.ExecuteAsync("[dbo].[ShipAdd]", new{ pDataJson = json } ,commandType: CommandType.StoredProcedure)
                    .ConfigureAwait(false);
            }
        }
    }
}