﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;
using SeaBattle.Domain;
using SeaBattle.Domain.Abstractions;
using SeaBattle.Domain.Entity;

namespace SeaBattle.Infrastructure.Repositories
{
    public class ShotRepository : IShotRepository
    {
        private readonly string _connectionString;

        public ShotRepository(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            _connectionString = connectionString;
        }

        public async Task<ShotStatEntity> CreateAsync(Coordinate coordinate)
        {
            if (coordinate == null)
            {
                throw new ArgumentNullException(nameof(coordinate));
            }

            var json = JsonConvert.SerializeObject(coordinate);

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync()
                    .ConfigureAwait(false);

                return (await connection.QueryAsync<ShotStatEntity>("[dbo].[ShotAdd]", new { pDataJson = json }, commandType: CommandType.StoredProcedure)
                    .ConfigureAwait(false))
                    .FirstOrDefault();
            }
        }
    }
}