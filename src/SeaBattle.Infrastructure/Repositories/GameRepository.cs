﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using SeaBattle.Domain.Abstractions;
using SeaBattle.Domain.Entity;

namespace SeaBattle.Infrastructure.Repositories
{
    public class GameRepository : IGameRepository
    {
        private readonly string _connectionString;
        
        public GameRepository(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            _connectionString = connectionString;
        }

        public async Task CreateAsync(int range)
        {
            using (var connection  = new SqlConnection(_connectionString))
            {
                await connection
                    .OpenAsync()
                    .ConfigureAwait(false);

                await connection.ExecuteAsync("[dbo].[GameAdd]", new{ pRange = range }, commandType: CommandType.StoredProcedure)
                    .ConfigureAwait(false);
            }
        }

        public async Task<GameEntity> GetAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection
                    .OpenAsync()
                    .ConfigureAwait(false);

                return (await connection.QueryAsync<GameEntity>("[dbo].[GameGet]", commandType: CommandType.StoredProcedure)
                        .ConfigureAwait(false))
                    .FirstOrDefault();
            }
        }

        public async Task<int> DeleteAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection
                    .OpenAsync()
                    .ConfigureAwait(false);

                return (await connection.QueryAsync<int>("[dbo].[GameDelete]", commandType: CommandType.StoredProcedure)
                        .ConfigureAwait(false))
                    .FirstOrDefault();
            }
        }
    }
}