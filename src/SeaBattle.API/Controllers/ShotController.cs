﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SeaBattle.API.Application.DTO;
using SeaBattle.Domain;
using SeaBattle.Domain.Abstractions;
using SeaBattle.Infrastructure.Utils;

namespace SeaBattle.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShotController : ControllerBase
    {
        private readonly IShotRepository _shotRepository;

        public ShotController(IShotRepository shipRepository)
        {
            _shotRepository = shipRepository ?? throw new ArgumentNullException(nameof(shipRepository));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ShotDTO shot)
        {
            if (!CoordinateUtility.TrySingleParse(shot.Coordinate, out var coord))
            {
                return BadRequest("Invalid coordinate");
            }

            var result = await _shotRepository.CreateAsync(new Coordinate(coord.x, coord.y));

            return Accepted(result);
        }
    }
}