﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SeaBattle.API.Application.DTO;
using SeaBattle.Domain.Abstractions;

namespace SeaBattle.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {
        private readonly IGameRepository _gameRepository;
        public GameController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository ?? throw new ArgumentNullException(nameof(gameRepository));
        }

        [HttpPost("create-matrix")]
        public async Task<IActionResult> CreateAsync([FromBody] RangeDTO range)
        {
            await _gameRepository.CreateAsync(range.Range);

            return StatusCode((int)HttpStatusCode.Created);
        } 
        
        [HttpGet("state")]
        public async Task<IActionResult> GetStateAsync()
        {
            var state = await _gameRepository.GetAsync();

            if (state == null)
            {
                return NoContent();
            }

            return Ok(state);
        }

        [HttpPost("clear")]
        public async Task<IActionResult> ClearAsync()
        {
            var state = await _gameRepository.DeleteAsync();

            return Accepted(state);
        }
    }
}