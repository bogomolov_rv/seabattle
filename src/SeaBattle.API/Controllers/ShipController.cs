﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SeaBattle.API.Application.DTO;
using SeaBattle.Domain.Abstractions;
using SeaBattle.Domain.Data;
using SeaBattle.Infrastructure.Repositories;
using SeaBattle.Infrastructure.Utils;

namespace SeaBattle.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShipController : ControllerBase
    {
        private readonly IShipRepository _shipRepository;

        public ShipController(IShipRepository shipRepository)
        {
            _shipRepository = shipRepository ?? throw new ArgumentNullException(nameof(shipRepository));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ShipDTO ship)
        {
            if (!CoordinateUtility.TryParse(ship.Coordinates, out var result))
            {
                return BadRequest("Invalid coordinates");
            }

            var shipData = result.Select(x => new ShipData(x)).ToList();

            await _shipRepository.CreateAsync(shipData);
            
            return Accepted();
        }
    } 
}