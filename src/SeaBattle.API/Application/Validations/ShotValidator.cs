﻿using FluentValidation;
using SeaBattle.API.Application.DTO;

namespace SeaBattle.API.Application.Validations
{
    public class ShotValidator : AbstractValidator<ShotDTO>
    {
        public ShotValidator()
        {
            RuleFor(x => x.Coordinate).NotNull().NotEmpty()
                .WithMessage("Coordinate must be set");
        }
    }
}