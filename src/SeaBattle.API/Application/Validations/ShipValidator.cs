﻿using FluentValidation;
using SeaBattle.API.Application.DTO;

namespace SeaBattle.API.Application.Validations
{
    public class ShipValidator : AbstractValidator<ShipDTO>
    {
        public ShipValidator()
        {
            RuleFor(x => x.Coordinates).NotNull().NotEmpty()
                .WithMessage("Coordinates must be set");
        }
    }
}