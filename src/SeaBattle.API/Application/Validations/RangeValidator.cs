﻿using FluentValidation;
using SeaBattle.API.Application.DTO;

namespace SeaBattle.API.Application.Validations
{
    public class RangeValidator : AbstractValidator<RangeDTO>
    {
        public RangeValidator()
        {
            RuleFor(x => x.Range).GreaterThan(0)
                .WithMessage("Range must be greater than 0");
        }
    }
}