﻿namespace SeaBattle.API.Application.DTO
{
    public class ShotDTO
    {
        public string Coordinate { get; set; }
    }
}