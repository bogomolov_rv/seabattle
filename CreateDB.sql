USE [master]
GO
/****** Object:  Database [SeaBattleDb]    Script Date: 25.01.2021 0:20:03 ******/
CREATE DATABASE [SeaBattleDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SeaBatleDb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\SeaBatleDb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SeaBatleDb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\SeaBatleDb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [SeaBattleDb] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SeaBattleDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SeaBattleDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SeaBattleDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SeaBattleDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SeaBattleDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SeaBattleDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [SeaBattleDb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SeaBattleDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SeaBattleDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SeaBattleDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SeaBattleDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SeaBattleDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SeaBattleDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SeaBattleDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SeaBattleDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SeaBattleDb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SeaBattleDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SeaBattleDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SeaBattleDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SeaBattleDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SeaBattleDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SeaBattleDb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SeaBattleDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SeaBattleDb] SET RECOVERY FULL 
GO
ALTER DATABASE [SeaBattleDb] SET  MULTI_USER 
GO
ALTER DATABASE [SeaBattleDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SeaBattleDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SeaBattleDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SeaBattleDb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SeaBattleDb] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SeaBattleDb', N'ON'
GO
ALTER DATABASE [SeaBattleDb] SET QUERY_STORE = OFF
GO
USE [SeaBattleDb]
GO
/****** Object:  Table [dbo].[GameOnline]    Script Date: 25.01.2021 0:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameOnline](
	[ID] [int] NOT NULL,
	[Range] [tinyint] NOT NULL,
	[ShotCount] [smallint] NOT NULL,
	[ShipCount] [tinyint] NOT NULL,
	[Destroyed] [tinyint] NOT NULL,
	[Knocked] [tinyint] NOT NULL,
	[Deleted] [datetime2](0) SPARSE  NULL,
 CONSTRAINT [PK__GameOnli__3214EC278DB65C11] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ship]    Script Date: 25.01.2021 0:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ship](
	[ID] [tinyint] NOT NULL,
	[CoordinateX] [int] NOT NULL,
	[CoordinateY] [int] NOT NULL,
	[IsHit] [bit] NULL,
 CONSTRAINT [PK__Ship__3214EC2760A8F999] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[CoordinateX] ASC,
	[CoordinateY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[GameAdd]    Script Date: 25.01.2021 0:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[                                                                                                    ] 
--[ Description: Создание игры
--[                                                                                                    ]
CREATE procedure [dbo].[GameAdd]
    @pRange int
as set nocount                 on
   set xact_abort              on
   set ansi_nulls              on
   set ansi_warnings           on
   set ansi_padding            on
   set ansi_null_dflt_on       on
   set arithabort              on
   set quoted_identifier       on
   set concat_null_yields_null on
   set implicit_transactions   off
   set cursor_close_on_commit  off
   set transaction isolation level read committed
begin

    declare @LastGameID int
          , @Trancount int;

    if (@pRange <=0)
        throw 51000, 'IvalidRange', 1;

    if(exists (select 1
                 from [dbo].[GameOnline] 
                where Deleted is null))
        throw 51001, 'CompletePreviousGame', 1;

    set @Trancount = @@trancount;
    if(@Trancount = 0)
        begin tran;

        set @LastGameID = isnull((select max([go].ID)
                                    from [dbo].[GameOnline] [go] with (tablock)), 0);

        insert into GameOnline (ID, [Range], ShotCount, ShipCount, Destroyed, Knocked)
            select @LastGameID + 1
                 , @pRange
                 , cast(0 as bit)
                 , cast(0 as bit)
                 , cast(0 as bit)
                 , cast(0 as bit);

    if(@Trancount = 0)
        commit tran;

    return 0;

end
GO
/****** Object:  StoredProcedure [dbo].[GameDelete]    Script Date: 25.01.2021 0:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--[                                                                                                    ] 
--[ Description: Завершить игру
--[                                                                                                    ]
CREATE procedure [dbo].[GameDelete]
as set nocount                 on
   set xact_abort              on
   set ansi_nulls              on
   set ansi_warnings           on
   set ansi_padding            on
   set ansi_null_dflt_on       on
   set arithabort              on
   set quoted_identifier       on
   set concat_null_yields_null on
   set implicit_transactions   off
   set cursor_close_on_commit  off
   set transaction isolation level read committed
begin
    
    declare @Trancount          int
          , @Now       datetime2(0) = getutcdate();

    create table #DeletedGame
    (
        ID int
    );

    if (not exists(select 1 
                     from [dbo].[GameOnline] [go] 
                    where [go].Deleted is null))
        throw 51000, 'AlreadyDone', 1;

    set @Trancount = @@trancount;
    if(@Trancount = 0)
        begin tran;

        update [go]
           set [go].Deleted = @Now
        output inserted.ID
          into #DeletedGame
          from [dbo].[GameOnline] [go]
         where [go].Deleted is null;

         delete [s]
           from [dbo].[Ship] [s]
           

    if(@Trancount = 0)
        commit tran;

    select ID 
      from #DeletedGame;

    return 0; 
end
GO
/****** Object:  StoredProcedure [dbo].[GameGet]    Script Date: 25.01.2021 0:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[                                                                                                    ] 
--[ Description: Получить информацию по текущей игре
--[                                                                                                    ]
create procedure [dbo].[GameGet]
as set nocount                 on
   set xact_abort              on
   set ansi_nulls              on
   set ansi_warnings           on
   set ansi_padding            on
   set ansi_null_dflt_on       on
   set arithabort              on
   set quoted_identifier       on
   set concat_null_yields_null on
   set implicit_transactions   off
   set cursor_close_on_commit  off
   set transaction isolation level read committed
begin
    select [ID]        = [go].ID
         , [Range]     = [go].[Range]
         , [ShotCount] = [go].ShotCount
         , [ShipCount] = [go].ShipCount
         , [Destroyed] = [go].Destroyed
         , [Knocked]   = [go].Knocked
      from [dbo].[GameOnline] [go]
     where [go].Deleted is null
end
GO
/****** Object:  StoredProcedure [dbo].[ShipAdd]    Script Date: 25.01.2021 0:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[                                                                                                    ]
--[ Description:  Добаление кораблей
--[     @pDataJson: [
--[         {"Ship": [{"X": 1, "Y": 1},{"X": 4,"Y": 4}]},
--[         {"Ship": [{"X": 2, "Y": 2},{"X": 3,"Y": 3}]}
--[     ]
--[                                                                                                    ]
CREATE procedure [dbo].[ShipAdd]
    @pDataJson nvarchar(max)
as set nocount                 on
   set xact_abort              on
   set ansi_nulls              on
   set ansi_warnings           on
   set ansi_padding            on
   set ansi_null_dflt_on       on
   set arithabort              on
   set quoted_identifier       on
   set concat_null_yields_null on
   set implicit_transactions   off
   set cursor_close_on_commit  off
   set transaction isolation level read committed
begin

    declare @Range     int
          , @Trancount int;

    if (isnull(isjson(@pDataJson), 0) = 0)
        throw 51000, 'InvalidData', 1;

    if(not exists (select 1 
                     from [dbo].[GameOnline]
                    where Deleted is null))
        throw 51000, 'GameNotFound', 1;

    if(exists (select 1 from [dbo].[Ship] [s]))
        throw 51000, 'NeedFinishOrClearGame', 1;

    create table #Ships
    (
          Number      tinyint       not null
        , Coordinates nvarchar(max) not null
    )

    create table #Ship
    (
          Number      tinyint not null
        , CoordinateX int     not null
        , CoordinateY int     not null
    )

    insert into #Ships(Number, Coordinates)
        select row_number() over (order by (select null))
             , s.Ships
          from openjson(@pDataJson)
          with (Ships  nvarchar(max) '$.Ship' as json) s;

    insert into #Ship (Number, CoordinateX, CoordinateY)
     select ss.Number
           , j.X
           , j.Y
       from #Ships ss
      cross apply openjson(ss.Coordinates)
       with ( X int 'strict $.X'
            , Y int 'strict $.Y') j

    if (not exists (select 1 from #Ship s))
        throw 51000, 'DataNotFound', 1;

    select top 1
           @Range = [go].[Range]
      from [dbo].[GameOnline] [go]
     where [go].Deleted is null;

    if(exists (select 1 
                 from Ship [s]
                where   [s].CoordinateX > @Range
                     or [s].CoordinateY > @Range))
        throw 51000, 'СoordinatesMustBeLessThanRange', 1;

    if(exists (select 1 
                from #Ship s 
               where exists (select 1 
                               from #Ship s1
                              where s1.Number <> s.Number
                                and s1.CoordinateX = s.CoordinateX
                                and s1.CoordinateY = s.CoordinateY)))
        throw 51000, 'ItersectOfCoordinates', 1;

    set @Trancount = @@trancount;
    if(@Trancount = 0)
        begin tran;

        insert into [dbo].[Ship] (ID, CoordinateX, CoordinateY, IsHit)
         select [s].Number
              , [s].CoordinateX
              , [s].CoordinateY
              , cast(0 as bit)
           from #Ship s

        update [go]
           set [go].ShipCount = (select max(s.Number) from #Ships s)
          from [dbo].[GameOnline] [go]
         where [go].Deleted is null

    if(@Trancount = 0)
        commit tran;

    return 0;
end
GO
/****** Object:  StoredProcedure [dbo].[ShotAdd]    Script Date: 25.01.2021 0:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--[                                                                                                    ] 
--[ Description: Сделать выстрел
--[     @pDataJson : {"X": 1, "Y": 1}
--[                                                                                                    ]
CREATE procedure [dbo].[ShotAdd]
    @pDataJson nvarchar(max)
as set nocount                 on
   set xact_abort              on
   set ansi_nulls              on
   set ansi_warnings           on
   set ansi_padding            on
   set ansi_null_dflt_on       on
   set arithabort              on
   set quoted_identifier       on
   set concat_null_yields_null on
   set implicit_transactions   off
   set cursor_close_on_commit  off
   set transaction isolation level read committed
begin
    declare @Trancount    int
          , @IsDestroy    bit = 0
          , @KnockCount   int = -1
          , @KnockUpdated bit = 0
          , @Range        int;

    if (isnull(isjson(@pDataJson), 0) = 0)
        throw 51000, 'InvalidData', 1;

    if(not exists (select 1 
                     from [dbo].[GameOnline]
                    where Deleted is null))
        throw 51000, 'GameNotFound', 1;

    /* "живых" кораблей нет */
    if(not exists (select 1 
                     from [dbo].[Ship] s 
                    where s.IsHit = 0))
        throw 51000, 'AllShipsDestroyed', 1;

    create table #Shot
    (
          X int  not null
        , Y int  not null
    );

    create table #KnockShip
    (
        ID  tinyint
    );

    insert into #Shot (X, Y)
        select [j].X
             , [j].Y
          from openjson(@pDataJson)
          with ( X int 'strict $.X'
               , Y int 'strict $.Y') j;

    if (not exists (select 1 from #Shot s))
        throw 51000, 'DataNotFound', 1;

    if(exists(select 1 
                from #Shot s
               where s.X <= 0
                  or s.Y <= 0))
        throw 51000, 'InvalidCoordinate', 1;

    select top 1
           @Range = [go].[Range]
      from [dbo].[GameOnline] [go]
     where [go].Deleted is null;

    if(exists (select 1 
                 from #Shot s
                where   [s].X > @Range
                     or [s].Y > @Range))
        throw 51000, 'ShotСoordinateMustBeLessThanRange', 1;

    /* уже был выстрел по этим координатам */
    if(exists (select 1 
                 from Ship s 
                where exists (select 1 
                                from #Shot ss 
                               where s.CoordinateX = ss.X
                                 and s.CoordinateY = ss.Y)
                         and s.IsHit = 1))
        throw 51000, 'DuplicateShot', 1;

    set @Trancount = @@trancount;
    if(@Trancount = 0)
        begin tran;

        update [s]
           set [s].IsHit = cast (1 as bit)
        output inserted.ID
          into #KnockShip(ID)
          from [dbo].Ship  [s]
          join #Shot      [s1] on [s].CoordinateX = [s1].X
                              and [s].CoordinateY = [s1].Y;

        /* Проверим, уничтожен ли корабль? */
        if (exists (select 1 
              from #KnockShip ks
             where not exists (select 1
                                 from [dbo].[Ship] s with(nolock)
                                where [s].ID    = [ks].ID
                                  and [s].IsHit = 0)))
        begin
            set @IsDestroy = 1;
        end;

         /* Пересчитаем количество поврежденных. */
        if(    @IsDestroy = 0
           and exists (select 1 from #KnockShip))
        begin
            select @KnockCount   = count(distinct s.ID)
              from [dbo].[Ship] [s] with(nolock)
             where s.IsHit = 1
               and exists(select 1
                            from Ship [ss] with(nolock)
                           where s.ID = ss.ID
                             and ss.IsHit = 0);
                
        end;

        update [go]
           set [go].ShotCount += 1
             , [go].Destroyed = iif(@IsDestroy  =  1, [go].Destroyed + 1, [go].Destroyed)
             , [go].Knocked   = case when @IsDestroy = 1 and [go].Knocked > 0 then [go].Knocked - 1
                                     when @KnockCount <> -1 then @KnockCount
                                     else [go].Knocked
                                end
          from [dbo].[GameOnline] [go]
         where [go].Deleted is null;

    if(@Trancount = 0)
        commit tran;

    select top 1 
           [Destroy] = @IsDestroy
         , [Knock]   = iif (exists (select 1 from #KnockShip us), 1, 0)
         , [End]     = iif([go].ShipCount = [go].Destroyed, 1, 0)
      from GameOnline [go] with(nolock)
     where [go].Deleted is null;

end
GO
USE [master]
GO
ALTER DATABASE [SeaBattleDb] SET  READ_WRITE 
GO
